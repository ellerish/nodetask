const fs = require('fs')
const os = require('os')
const http = require('http')

const app = require("./app");
const port = 8080;
const server = require('http').Server(app);



const option1 = "1. Read package.json"
const option2 = "2. Display OS information"
const option3 = "3. Start HTTP Server"

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
  console.log("Choose an option: (1, 2, 3) ")
  console.log(option1, "\n" ,option2, "\n", option3)
  readline.question('Type a number?', numOption => {
    console.log(`You choose ${numOption}!`);
    numOption = Number(numOption)
    if(numOption === 1) {
        readJson() 
    } if (numOption === 2) {
        readOs()
    } if(numOption === 3) {
        httpHello()
    } else {
        console.log('Invalid option')
    }
    readline.close();

  });

  function readJson(){
    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content)
    })
  }

  function readOs(){
    console.log( 'SYSTEM Memory ', (os.totalmem() / 1024 / 1024 /1024).toFixed(2) + ' GB')
    console.log( 'FREE Memory ', (os.freemem() / 1024 / 1024 /1024).toFixed(2) + ' GB')
    console.log( 'CPU CORES ' + os.cpus())
    console.log( 'Architecture: ' + os.arch() )
    console.log( 'PLATFORM',  + os.platform())
    console.log( 'REALEASE: ' + os.release())
    console.log( 'USER: ' +  os.userInfo().username)
  }

function httpHello() {
    server.listen(port, () => {
        console.log('Starting server on port ' + port);
    });
    
}

